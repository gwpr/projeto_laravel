<?php

namespace App\Http\Controllers\Admin;

use App\Restaurant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RestaurantController extends Controller
{
    public function index()
    {
        return __CLASS__;
    }

    public function new()
    {
        return view('admin.restaurants.store');
    }

    public function store(Request $request)
    {
       $restauranteData = $request->all();

       $restaurante=new Restaurant();
       $restaurante->create($restauranteData);

       print 'Restaurante Criado';
    }

}
