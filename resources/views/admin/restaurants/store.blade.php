<h1>Inserindo um Restaurante</h1>
<hr>

<form action="{{route('restaurant.store')}}" method="post">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <p>
        <label>Nome do Restaurante</label>
        <input type="text" name="name">
    </p>

    <p>
        <label>Endereço</label>
        <input type="text" name="address">
    </p>

    <p>
        <label>Fale sobre o restaurante</label>
        <textarea type="text" name="description" cols="30" rows="10"></textarea>
    </p>

    <input type="submit" value="Cadastrar">
</form>